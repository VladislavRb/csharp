﻿using System;

namespace Task1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("temperature in celsius: ");
            double temperatureInCelsius = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Speed in m/s: ");
            double speedInMpS = Convert.ToDouble(Console.ReadLine());

            double temperatureInFahrenheit = temperatureInCelsius * 1.8 + 32;
            double speedInMpH = speedInMpS * 2.25;
            double effectiveTemperature = 35.74 + 0.6215 * temperatureInFahrenheit +
                                          (0.4275 * temperatureInFahrenheit - 35.75) * Math.Pow(speedInMpH, 0.16);
            Console.WriteLine(effectiveTemperature);

            if (Math.Abs(temperatureInFahrenheit) > 50 || speedInMpH < 3 || speedInMpH > 120)
            {
                Console.WriteLine("Data might be incorrect");
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace Task1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("start: ");
            int a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("end: ");
            int b = Convert.ToInt32(Console.ReadLine());

            for (int number = a; number <= b; number++)
            {
                if (number > 0)
                {
                    string binary = Convert.ToString(number, 2);
                    int ones = binary.Count(ch => ch == '1');
                    if (ones == 4)
                    {
                        Console.Write(number);
                        Console.Write(" ");
                    }
                }
            }
        }
    }
}
